//
//  WEbViewController.swift
//  webView
//
//  Created by Mehmed Kadir on 2.05.18.
//  Copyright © 2018 Mehmed Kadir. All rights reserved.
//

import UIKit
import WebKit

class WEbViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    
    var url:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
         let urlR = URL (string:url!)
         let requestObj = URLRequest(url: urlR!)
        print(requestObj)
        webView.load(requestObj)
    }
}
