//
//  ViewController.swift
//  webView
//
//  Created by Mehmed Kadir on 2.05.18.
//  Copyright © 2018 Mehmed Kadir. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var url: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func test(_ sender: Any) {
        guard url.text != "" else {return}
        if let vc = storyboard?.instantiateViewController(withIdentifier: "WEbViewController") as? WEbViewController {
            vc.url = url.text!
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

